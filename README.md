This Project provide extraction of emotions and other facial features by using microsoft face api (ms api) [1]


Instruction for executing the project:
Considering that the the project is located  in the home directory.

"execute_face_api.sh" is the executable, resides at "ms_face_api/src/", which start the extraction of the emotion
and facial features of images and then saves a csv file with the extracted values.
Each row of the csv file has one extracted feature of one face in the image, in addition to the extracted features
every row has a "image_id", which is the name of the image, if one image has more than one face in it they can be traced
by the same image_id.

Main parameter which needed to be updated:

The absolute path of the list which as images paths inside it can be provided in the executable with the parameter "-images_names_list_file"
similar to one dummy file "list_of_files.txt"
The absolute path with file name to be saved as csv can be provided with parameter"-csv"


Other parameter to be considered:

The keys can be pass as  space separated strings "key1 key2 key3" with parameters "-keys"
There are already one key which is ready to use.
The Keys has been generated with putting region to "northeurope" so if more keys need to be added, they should be
generated with region selected as northeurope for the endpoint URL from microsoft.

-chunk_size, chunk size is the size of the chunk of extracted row values to be saved at once in the file, if no
chunk size is provided it save all row at once in the file, it has been added for avoiding memory issue in case of high
number of images.

-stc parameter only need to be change to 20 if the key is from free api which has 300,000 call but 20 max call in one minutes.
The provided key is paid account which do not have this limit so the default value of -stc is 0

One dummy csv file called face_emotion.csv has been also attached in the project.
The name, in general, separated by "_" e.g. "emotion_anger" so anger is one of the properties of emotions.

References:
1. https://azure.microsoft.com/en-us/services/cognitive-services/face/