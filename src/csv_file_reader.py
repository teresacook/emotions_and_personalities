import pandas as pd
import numpy as np


LIST_ = ["emotion_anger", "emotion_contempt", "emotion_disgust", "emotion_fear", "emotion_happiness",
         "emotion_neutral", "emotion_sadness", "emotion_surprise", "faceAttributes_age", "faceAttributes_gender",
         "faceAttributes_glasses", "faceAttributes_smile", "filename", "userid",
         "ope", "con", "ext", "agr", "neu", "item_level", "gender",
         "age", "locale", "relationship_status", "facialHair_beard", "facialHair_moustache", "facialHair_sideburns",
         "hair_bald", "hair_black", "hair_blond", "hair_brown", "hair_gray", "hair_invisible",
         "hair_other", "hair_red"]


def get_the_main_emotion(data_frame):
    data_frame_emotions = extract(
        ["emotion_anger", "emotion_contempt", "emotion_disgust", "emotion_fear", "emotion_happiness",
         "emotion_neutral", "emotion_sadness", "emotion_surprise"], data_frame)
    data_frame_filname_userid = extract(["filename", "userid"], data_frame)
    data_frame_filname_userid.loc[:, "main_emotion"] = data_frame_emotions.idxmax()
    data_frame_filname_userid.to_csv("main_emotion.csv")
    print("something....")


def read_csv(path_to_csv_file):
    data_frame = pd.read_csv(path_to_csv_file)
    print("Complete data: ", data_frame.shape)
    smaller_data = extract(LIST_, data_frame)
    duplicated_dropped = remove_multi_file_name(smaller_data)
    print("Duplicated dropped : ", duplicated_dropped.shape)
    # get_the_main_emotion(duplicated_dropped)

    counter_added = count_profile_pics(data_frame)

    most_recent = keep_most_recent_image(counter_added)
    # most_recent = duplicated_dropped
    print("most recent image : ", most_recent.shape)

    gender_filter_ = gender_filter(most_recent)

    age_filtered_ = age_filter(gender_filter_, min_age=14, max_diff=20)
    #gender_filter_ = gender_filter(age_filtered_)

    print("Age Filtered: ", age_filtered_.shape)
    print("Gender Filtered: ", gender_filter_.shape)

    relationship_converted = relationship_status(age_filtered_)
    print("Relationship Status Transformed: ", relationship_converted.shape)
    transformed_glasses_df = transform_glasses(relationship_converted)
    print("Glasses Transformed: ", transformed_glasses_df.shape)

    transform_locale_df = transform_locale(transformed_glasses_df)
    print("transformed locale : ", transform_locale_df.shape)
    # de_skew(data_frame, input_list):

    transform_locale_df = de_skew(transform_locale_df, ["emotion_anger", "emotion_contempt", "emotion_disgust",
                                                        "emotion_fear", "emotion_happiness", "emotion_neutral",
                                                        "emotion_sadness", "emotion_surprise"])

    transform_locale_df.to_csv("cleaned_data_for_teresa_unique_age14.csv")

    get_quantiles(transform_locale_df, "cleaned_data_for_teresa_unique_age14_alles")

    save_with_relation_status_in_or_out(transform_locale_df, "both_gender")

    male_data_frame = get_gender(transform_locale_df, 0)
    print("male : ", male_data_frame.shape)
    male_data_frame.to_csv("cleaned_data_for_teresa_unique_age14_male.csv")
    get_quantiles(male_data_frame, "cleaned_data_for_teresa_unique_age14_male")
    # save_with_relation_status(male_data_frame, "male")
    save_with_relation_status_in_or_out(male_data_frame, "male")

    female_data_frame = get_gender(transform_locale_df, 1)
    print("female : ", female_data_frame.shape)
    female_data_frame.to_csv("cleaned_data_for_teresa_unique_age14_female.csv")
    get_quantiles(female_data_frame, "cleaned_data_for_teresa_unique_age14_female")
    # save_with_relation_status(female_data_frame, "female")
    save_with_relation_status_in_or_out(female_data_frame, "female")


def get_quantiles(data_frame, prefix):
    personalities_ = ["ope", "con", "ext", "agr", "neu"]
    for personality_ in personalities_:
        max_value = data_frame[personality_].max()
        quartile_value_25 = np.percentile(data_frame[personality_], 25)
        data_frame_to_save = data_frame.loc[(data_frame[personality_] >= 0.0) & (data_frame[personality_] <
                                                                                 quartile_value_25)]
        data_frame_to_save.to_csv(prefix + "_" + personality_ + "_" + str(25) + ".csv")

        quartile_value_50 = np.percentile(data_frame[personality_], 50)
        data_frame_to_save = data_frame.loc[
            (data_frame[personality_] >= quartile_value_25) & (data_frame[personality_] <
                                                               quartile_value_50)]
        data_frame_to_save.to_csv(prefix + "_" + personality_ + "_" + str(50) + ".csv")

        quartile_value_75 = np.percentile(data_frame[personality_], 75)
        data_frame_to_save = data_frame.loc[
            (data_frame[personality_] >= quartile_value_50) & (data_frame[personality_] <
                                                               quartile_value_75)]
        data_frame_to_save.to_csv(prefix + "_" + personality_ + "_" + str(75) + ".csv")

        data_frame_to_save = data_frame.loc[
            (data_frame[personality_] >= quartile_value_75) & (data_frame[personality_] <
                                                               max_value)]
        data_frame_to_save.to_csv(prefix + "_" + personality_ + "_" + str(100) + ".csv")

        data_frame_to_save = data_frame.loc[((data_frame[personality_] >= 0.0) & (data_frame[personality_] <
                                                                                  quartile_value_25)) |
                                            (data_frame[personality_] >= quartile_value_75) & (
                                                data_frame[personality_] <
                                                max_value)]
        data_frame_to_save.to_csv(prefix + "_" + personality_ + "_" + str(25) + "_" + str(100) + ".csv")

        data_frame_to_save.loc[:, personality_ + "class"] = np.where(data_frame_to_save[personality_] <= quartile_value_25, 0, 1)
        data_frame_to_save.to_csv(prefix + "_" + personality_ + "class_" + str(25) + "_" + str(100) + ".csv")


def get_gender(data_frame, value):
    return data_frame.loc[(data_frame.gender == value)]


def save_with_relation_status(data_frame, gender):
    for key_, value_ in RELATIONSHIP_STATUS.items():
        data_frame_ = data_frame.loc[(data_frame[value_] == 1)]
        data_frame_.to_csv("cleaned_data_" + gender + "_" + value_ + ".csv")


def save_with_relation_status_in_or_out(data_frame, gender):
    data_frame_ = data_frame.loc[
        (data_frame["Divorced"] == 1) | (data_frame["Separated"] == 1) | (data_frame["Widowed"] == 1)
        | (data_frame["Single"] == 1) | (data_frame["It's Complicated"] == 1)]
    data_frame_.to_csv("cleaned_data_" + gender + "_not_in_relationship" + ".csv")

    name_suffix = gender + "_not_in_relationship"
    save_with_glasses(data_frame_, name_suffix)
    get_quantiles(data_frame_, name_suffix)

    data_frame_ = data_frame.loc[
        (data_frame["Divorced"] == 0) & (data_frame["Separated"] == 0) & (data_frame["Widowed"] == 0) & (
            data_frame["Reationship_NA"] == 0)
        & (data_frame["Single"] == 0) & (data_frame["It's Complicated"] == 0)]
    data_frame_.to_csv("cleaned_data_" + gender + "_in_relationship" + ".csv")

    name_suffix = gender + "_in_relationship"
    save_with_glasses(data_frame_, name_suffix)
    get_quantiles(data_frame_, name_suffix)


def age_filter(data_frame, min_age=14, max_diff=20):
    age_filtered_ = data_frame.loc[(data_frame.faceAttributes_age >= min_age) & (data_frame.age >= 14) &
                                   ((data_frame.age - data_frame.faceAttributes_age).abs() < max_diff)]

    return age_filtered_


RELATIONSHIP_STATUS = {1: "Single", 2: "In a Relationship", 3: 'Married', 4: "Engaged",
                       5: "It's Complicated", 6: "In an Open Relationship", 7: "Widowed", 8: "Divorced",
                       9: 'Separated', 10: 'In a domestic partnership', 11: 'In a Civil Union', 12: 'Hooked'}


def relationship_status(data_frame):
    for key, value in RELATIONSHIP_STATUS.items():
        data_frame.loc[:, value] = np.where(data_frame["relationship_status"] == key, 1, 0)
    # Nan has to be added separately.
    # "nan": ""nan": "Reationship_NA""
    data_frame["relationship_status"] = data_frame["relationship_status"].fillna(13)
    data_frame.loc[:, "Reationship_NA"] = np.where(data_frame["relationship_status"] == 13, 1, 0)
    data_frame.drop("relationship_status", axis=1, inplace=True)
    return data_frame


# all_glasses = data_frame.faceAttributes_glasses.unique()  # extract the values once already
GLASSESS_VALUES = ['NoGlasses', 'Sunglasses', 'ReadingGlasses', 'SwimmingGoggles']


def transform_glasses(data_frame):
    for index, value_ in enumerate(GLASSESS_VALUES):
        data_frame.loc[:, value_] = np.where(data_frame["faceAttributes_glasses"] == value_, 1, 0)
    data_frame.drop("faceAttributes_glasses", axis=1, inplace=True)
    return data_frame


def save_with_glasses(data_frame, gender):
    for value_ in GLASSESS_VALUES:
        data_frame_ = data_frame.loc[(data_frame[value_] == 1)]
        data_frame_.to_csv("cleaned_data_" + gender + "_" + value_ + ".csv")


def gender_filter(data_frame):
    data_frame.loc[:, "corrected_gender"] = np.where(data_frame["faceAttributes_gender"] == 'male', 0, 1);
    data_frame.loc[:, "faceAttributes_gender"] = data_frame["corrected_gender"]
    data_frame.drop('corrected_gender', axis=1, inplace=True)
    gender_filter_ = data_frame.loc[data_frame.gender == data_frame.faceAttributes_gender];

    return gender_filter_


def transform_locale(data_frame):
    # all_locale = data_frame.locale.unique()  # extract the values once already
    all_locale_ = ['en_US', 'en_GB', 'es_LA', 'lang_NA', 'fb_LT', 'en_PI', 'sv_SE', 'it_IT', 'de_DE',
                   'pt_BR', 'pt_PT', 'nl_NL', 'ru_RU', 'fi_FI', 'fr_FR', 'nb_NO', 'en_UD', 'en_IN',
                   'fr_CA', 'id_ID', 'es_ES', 'bg_BG', 'tl_PH', 'el_GR', 'da_DK', 'pl_PL', 'hu_HU',
                   'zh_HK', 'sq_AL', 'la_VA', 'zh_TW', 'tr_TR', 'sr_RS', 'ro_RO', 'ja_JP', 'sl_SI',
                   'lt_LT', 'vi_VN', 'af_ZA', 'zh_CN', 'ar_AR', 'ca_ES', 'sk_SK', 'ko_KR', 'hr_HR',
                   'et_EE', 'cs_CZ', 'mt_MT', 'is_IS', 'eu_ES', 'th_TH', 'nl_BE', 'fo_FO', 'mk_MK',
                   'nn_NO', 'es_VE', 'gl_ES', 'qu_PE', 'kn_IN', 'es_CL', 'es_MX', 'ga_IE', 'ne_NP',
                   'cy_GB', 'he_IL', 'ms_MY', 'bn_IN', 'bs_BA', 'uk_UA', 'eo_EO']

    data_frame["locale"] = data_frame["locale"].fillna('lang_NA')
    for index, value_ in enumerate(all_locale_):
        data_frame.loc[:, value_] = np.where(data_frame["locale"] == value_, 1, 0)

    data_frame.drop("locale", axis=1, inplace=True)
    return data_frame


def count_profile_pics(data_frame):
    df_series = data_frame.groupby("userid").userid.count()

    counter = lambda x: df_series.get(x)
    data_frame.loc[:, "pofile_pic_count"] = data_frame["userid"].apply(counter)
    return data_frame


def remove_multi_file_name(data_frame):
    data_frame.drop_duplicates("filename", keep=False, inplace=True)
    return data_frame


def keep_most_recent_image(data_frame):
    data_frame.drop_duplicates("userid", keep="last", inplace=True)
    return data_frame


def extract(input_list, data_frame):
    set_ = set(input_list)
    all_the_keys = data_frame.keys()
    for key in all_the_keys:
        if key not in set_:
            data_frame.drop(key, axis=1, inplace=True)
    return data_frame


def de_skew(data_frame, input_list):
    logged_ = lambda x: np.log(x + 1.0)
    for entry_ in input_list:
        data_frame.loc[:, entry_ + "_de_skewed"] = data_frame[entry_].apply(logged_)
    return data_frame


if __name__ == '__main__':
    read_csv("/Users/teresa/Downloads/data_prepared_for_teresa_2.csv")
