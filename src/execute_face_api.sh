#!/usr/bin/env bash


cd $PBS_O_WORKDIR

pip install -r ${HOME}/ms_face_api/requirements.txt

python ${HOME}/ms_face_api/src/ms_face_api.py -max_call 132000 -stc 0 -images_names_list_file=${HOME}"/ms_face_api/list_of_files.txt"\
                             -csv "face_emotion.csv" -chunk_size 0\
                             -face_att_extract "age,gender,smile,glasses,headPose,facialHair,emotion,hair,makeup,occlusion,accessories,blur,exposure,noise"\
                             -url 'https://northeurope.api.cognitive.microsoft.com/face/v1.0/detect'\
                             -keys "Add your key you get form ms face API"\



#######################
#parser.add_argument('-images_names_list_file', type=str, help="Path to the file which has images path in it.")
#parser.add_argument('-face_att_extract', type=str, help="Pass a comma separated string e.g. age,gender,smile")
#parser.add_argument('-face_ID', type=bool, default=False, help="True if one wants to get the faceId.")
#parser.add_argument('-url', type=str, help="ms face api URL.")
#parser.add_argument('-keys', type=list, help="keys to be used one by one, a comma separted single string e.g."key1, key2" ")
#parser.add_argument('-face_landmarks', type=bool,default=False, help="True if face landmarks to be extracted.")
#parser.add_argument('-chunk_size', type=int, help="The chunk of extracted data to be saved at once, for memory issues"
#                                                        "if no value is passed it will save all data at once.")
                                                         #we use pandas to save the csv file.
#parser.add_argument('-max_call', type=int, default=132000, help="The maximum number of request with one key")
#parser.add_argument('-csv', type=str, default="emotions.csv", help="Absolute path of the csv file to be saved.")
#parser.add_argument('-stc', type=int, default=0, help="The maximum number of requests in 60 seconds.")#######################



#the second keys
#bfbff81e3f1a43649da617dc492497a3
#88571a7b98354de5809f309c6012b56e
#7badea1470e54e75b5e75374d46b1def