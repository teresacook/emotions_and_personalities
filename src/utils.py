import os
import sys

basedir = os.path.abspath(os.path.dirname(__file__))
sys.path.insert(0, basedir)

from .DataKeys import KeysFaceLandmarks, KeyAccessories, KeyOcclusions, \
    KeysFcaeRectangles, KeysHairs, KeysFaceAttributes, KeysNoise, KeysMakeup


def add_prefix(prefix, other):
    return prefix + "_" + other


def extract_face_landmarks(input_json, key_prefix):
    face_landmarks = dict()
    for key in KeysFaceLandmarks().all_keys():
        key_value = input_json.get(key)
        x_value = 0.0
        y_value = 0.0
        if key_value:
            x_value = key_value.get(KeysFaceLandmarks.x, 0.0)
            y_value = key_value.get(KeysFaceLandmarks.y, 0.0)
        face_landmarks.update({add_prefix(key_prefix, KeysFaceLandmarks.x): x_value})
        face_landmarks.update({add_prefix(key_prefix, KeysFaceLandmarks.y): y_value})
    return face_landmarks


def extract_values(input_json, object):
    '''
    object can be, KeysEmotions, KeyHeadPose, KeyFacialHair
    :param input_json:
    :param object:
    :return:
    '''
    values = dict()
    inner_data = input_json.get(object.outer_key())
    for key in object.all_keys():
        values.update({add_prefix(object.outer_key(), key): inner_data.get(key, 0.0)})
    return values


def extract_occlusion(input_json, key_prefix):
    occlusion = dict()
    inner_data = input_json.get(key_prefix)
    for key in KeyOcclusions().all_keys():
        occlusion.update({add_prefix(key_prefix, key): input_json.get(key, False)})
    return occlusion


def extract_makeup(input_json, key_prefix):
    makeups = dict()
    inner_data = input_json.get(key_prefix)
    for key in KeysMakeup().all_keys():
        makeups.update({add_prefix(key_prefix, key): inner_data.get(key, False)})
    return makeups


def extract_face_rectangle(input_json, key_prefix):
    face_rectangle = dict()
    for key in KeysFcaeRectangles().all_keys():
        face_rectangle.update({add_prefix(key_prefix, key): input_json.get(key, 0.0)})
    return face_rectangle


def extract_hair(input_json, key_prefix):
    """"hair": {
                "bald": 0.0,
                "invisible": false,
                "hairColor": [
                    {"color": "brown", "confidence": 1.0},
                    {"color": "blond", "confidence": 0.88},
                    {"color": "black", "confidence": 0.48},
                    {"color": "other", "confidence": 0.11},
                    {"color": "gray", "confidence": 0.07},
                    {"color": "red", "confidence": 0.03}
                ]
            },
    hair: return face features indicating whether the hair is visible, bald or not also including hair color if available.
    """
    hair_dict = dict()
    inner_data = input_json.get(key_prefix)
    for key in KeysHairs().independent_key():
        if key == KeysHairs.bald:
            hair_dict.update({add_prefix(key_prefix, key): inner_data.get(key, 0.0)})
        else:
            hair_dict.update({add_prefix(key_prefix, key): inner_data.get(key, False)})

    color_dic_local = {}
    hair_color = KeysHairs().hairColor
    dependent_keys = KeysHairs().dependent_keys(hair_color)
    _inside_list = inner_data.get(hair_color)
    if _inside_list:
        for entry in _inside_list:
            color_ = entry.get(dependent_keys[0])
            confidence_ = entry.get(dependent_keys[1])
            if color_ and confidence_:
                color_dic_local.update({color_: confidence_})
    # now extracting the value  for each color
    for color_ in KeysHairs().color_list():
        hair_dict.update({add_prefix(key_prefix, color_): color_dic_local.get(color_, 0.0)})

    return hair_dict


def extract_face_attributes(input_json, key_prefix):
    '''
    "faceAttributes": {
                          "age": 71.0,
                          "gender": "male",
                          "smile": 0.88,
                          "facialHair": {
                              "moustache": 0.8,
                              "beard": 0.1,
                              "sideburns": 0.02
                          }
                      },
    "glasses": "sunglasses",
    "headPose": {
                    "roll": 2.1,
                    "yaw": 3,
                    "pitch": 0
                },
    '''
    face_attributes_dict = dict()
    for key in KeysFaceAttributes().all_keys():
        if key in [KeysFaceAttributes.gender, KeysFaceAttributes.glasses]:
            face_attributes_dict.update({add_prefix(key_prefix, key): input_json.get(key, "other")})
        else:
            face_attributes_dict.update({add_prefix(key_prefix, key): input_json.get(key, 0.0)})

    return face_attributes_dict


def extract_accessories(input_json, key_prefix):
    '''"accessories":[
    {"type": "headWear", "confidence": 0.99},
    {"type": "glasses", "confidence": 1.0},
    {"type": "mask", " confidence": 0.87}
    ]
    An empty array means no accessories have been found.
    '''
    color_dic_local = {}
    accessories = dict()
    _all_list = input_json.get(KeyAccessories().outer_key())
    if _all_list:
        type_ = input_json.get(KeyAccessories.type)
        confidence_ = input_json.get(KeyAccessories.confidence)
        if type_ and confidence_:
            color_dic_local.update({type_: confidence_})

    for entry in KeyAccessories().all_keys():
        accessories.update({add_prefix(key_prefix, entry): color_dic_local.get(entry, 0.0)})

    return accessories


def extract_noise_level(input_json, key_prefix):
    return {add_prefix(key_prefix, "level"): input_json.get(KeysNoise.value, 0.0)}


def extract_exposuer_level(input_json, key_prefix):
    return {add_prefix(key_prefix, "level"): input_json.get(KeysNoise.value, 0.0)}


def extract_blur_level(input_json, key_prefix):
    return {add_prefix(key_prefix, "level"): input_json.get(KeysNoise.value, 0.0)}
