import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from src.csv_file_reader import extract



data_frame = pd.read_csv("cleaned_data_for_teresa_unique_age14.csv")

traits = ["ope", "ext", "con", "neu", "agr"]

data_frame_traits = extract(traits, data_frame)

data_frame_traits.rename(columns={"ope":"Openness", "ext":"Extroversion", "con":"Conscientiousness", "neu":"Neuroticism", "agr":"Agreeableness"}, inplace=True)
#data_frame_traits = extract(["Openness", "Conscientiousness","Agreeableness", "Extroversion", "Neuroticism"])
color = dict(boxes='DarkGreen', whiskers='DarkOrange',medians='DarkBlue', caps='Gray')
#data_frame_ope = data_frame["e "]
plt.figure()
data_frame_traits.plot.box(color=color, sym='')
plt.xlabel("Personalty Traits")
plt.ylabel("Scores")
plt.savefig("Personalty_Traits.png")
plt.show()
