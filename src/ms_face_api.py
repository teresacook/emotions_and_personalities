#!/usr/bin/env python

from __future__ import print_function

import os
import sys
import re

import time
import argparse
import requests
import pandas as pd

basedir = os.path.abspath(os.path.dirname(__file__))


sys.path.insert(0, basedir)

from .DataKeys import *
from .utils import *

_MAXNUMRETRIES = 10

_URL = "https://northeurope.api.cognitive.microsoft.com/face/v1.0/detect"
_KEY = "Add your key here"
FACE_ATTRIBUTE_TO_GET = "age,gender,smile,glasses,headPose,facialHair,emotion,hair,makeup,occlusion,accessories,blur,exposure,noise"


def merge_two_dicts(x, y):
    z = x.copy()  # start with x's keys and values
    z.update(y)  # modifies z with y's keys and values & returns None
    return z


def json_to_csv_rows(input_json_list, image_name, face_attributes_set_):
    _all_faces_attribute = []

    if not input_json_list:
        # in any case something gose wrong.
        return _all_faces_attribute
        # i need to create a pipe line here. but I also have to keep track of the
        # now we need to create pipes of DataKeys
    for input_json in input_json_list:
        full_dict = {}
        full_dict.update({"image_id": image_name})
        face_rectangle_json = input_json.get(KeysFcaeRectangles().outer_key())
        face_rectangle_dict = extract_face_rectangle(face_rectangle_json, KeysFcaeRectangles().outer_key())

        full_dict = merge_two_dicts(full_dict, face_rectangle_dict)
        face_attributes_json = input_json.get(KeysFaceAttributes().outer_key())
        full_dict = merge_two_dicts(full_dict, extract_face_attributes(face_attributes_json,
                                                                       KeysFaceAttributes().outer_key()))
        if KeysHairs.outer_key() in face_attributes_set_:
            full_dict = merge_two_dicts(full_dict, extract_hair(face_attributes_json, KeysHairs.outer_key()))

        if KeysEmotions().outer_key() in face_attributes_set_:
            full_dict = merge_two_dicts(full_dict, extract_values(face_attributes_json, KeysEmotions()))

        if KeyHeadPose().outer_key() in face_attributes_set_:
            full_dict = merge_two_dicts(full_dict, extract_values(face_attributes_json, KeyHeadPose()))

        if KeyFacialHair().outer_key() in face_attributes_set_:
            full_dict = merge_two_dicts(full_dict, extract_values(face_attributes_json, KeyFacialHair()))

        if KeysMakeup().outer_key() in face_attributes_set_:
            full_dict = merge_two_dicts(full_dict, extract_makeup(face_attributes_json, KeysMakeup().outer_key()))

        if KeyAccessories().outer_key() in face_attributes_set_:
            full_dict = merge_two_dicts(full_dict,
                                        extract_accessories(face_attributes_json, KeyAccessories().outer_key()))

        if KeysNoise.noise in face_attributes_set_:
            full_dict = merge_two_dicts(full_dict,
                                        extract_noise_level(face_attributes_json.get(KeysNoise.noise), KeysNoise.noise))
        if KeysNoise.blur in face_attributes_set_:
            full_dict = merge_two_dicts(full_dict,
                                        extract_noise_level(face_attributes_json.get(KeysNoise.blur), KeysNoise.blur))
        if KeysNoise.exposure in face_attributes_set_:
            full_dict = merge_two_dicts(full_dict, extract_noise_level(face_attributes_json.get(KeysNoise.exposure),
                                                                       KeysNoise.exposure))
        _all_faces_attribute.append(full_dict)
    return _all_faces_attribute


def status_code_check_for_key(response):
    "need to check the error codes here., other codes are not realted for changing the key."
    if response.status_code == 403:
        return True
    if response.status_code == 200 or response.status_code == 201:
        return False
    else:
        code = response.json()["error"].get("code")
        if code and code == "Unspecified":
            return True
    return False


def process_face_api(json, URL_, data, headers, params):
    """
    Helper function to process the request to Project Oxford

    Parameters:
    json: Used when processing images from its URL. See API Documentation
    data: Used when processing image read from disk. See API Documentation
    headers: Used to pass the key information and the data type request
    """
    retries = 0
    result = None

    while True:

        response = requests.request('post', URL_, json=json, data=data, headers=headers, params=params)

        if response.status_code == 429:

            print("Message: %s" % (response.json()['error']['message']))

            if retries <= _MAXNUMRETRIES:
                time.sleep(1)
                retries += 1
                continue
            else:
                print('Error: failed after retrying!')
                break

        elif response.status_code == 200 or response.status_code == 201:

            if 'content-length' in response.headers and int(response.headers['content-length']) == 0:
                result = None
            elif 'content-type' in response.headers and isinstance(response.headers['content-type'], str):
                if 'application/json' in response.headers['content-type'].lower():
                    result = response.json() if response.content else None
                elif 'image' in response.headers['content-type'].lower():
                    result = response.content
        else:
            print("Error code: %d" % (response.status_code))
            print("Message: %s" % (response.json()['error']['message']))

        break

    boolean_flag = status_code_check_for_key(response)
    return result, boolean_flag


def sleep_if_needed(time_tracker, upper_limit):
    current_time = time.time()
    sleeping_time = current_time - time_tracker
    if sleeping_time < upper_limit:
        print("sleeping for %d" % sleeping_time)
        time.sleep(sleeping_time)
    return time.time()


def read_all_files_names(path_to_file):
    file_names = []
    with open(path_to_file, "r") as file:
        for file_name in file.readlines():
            file_names.append(file_name.strip())
    return file_names


def read_image_and_detect(path_to_images_name_file, face_attributes_to_get_=FACE_ATTRIBUTE_TO_GET, keys=_KEY, URL_=_URL,
                          path_to_csv_file="face_attributes.csv",
                          sleep_time_count=0,
                          maximum_calls_=132000, face_landmarks="false", face_id=False, chunk_size_file=0):
    # all_images = os.listdir(path_to_dir)
    all_images = read_all_files_names(path_to_images_name_file)
    print(all_images)
    headers = dict()
    headers['Content-Type'] = 'application/octet-stream'
    json = None
    params = {
        'returnFaceId': face_id,
        'returnFaceLandmarks': face_landmarks,
        'returnFaceAttributes': face_attributes_to_get_,
    }
    _face_attributes_set = set(face_attributes_to_get_.split(","))
    image_counter = 0
    keys = re.split("\\s+", keys)
    index_for_key = 0
    all_results = []
    one_minute_tracker = time.time()
    first_chunk = True
    if not chunk_size_file:
        chunk_size_file = len(all_images)
    all_files = 0
    one_sec_limit = time.time()
    boolean_flag_key = False
    for image in all_images:
        all_files += 1
        if image_counter % maximum_calls_ == 0 or boolean_flag_key:
            if index_for_key < len(keys):
                print("Updating the subscription_key : ", keys[index_for_key])
                headers['Ocp-Apim-Subscription-Key'] = keys[index_for_key].strip()
                index_for_key += 1
        if image.lower().endswith(('jpeg', 'jpg', 'png', "bmp")):
            image_counter += 1
            with open(image, 'rb') as image_file:
                data = image_file.read()
                if image_counter % 10 == 0:
                    # 1O maximum call can be made in 10 sec in all kind account
                    one_sec_limit = sleep_if_needed(one_sec_limit, 1)
                result, boolean_flag_key = process_face_api(json, URL_, data, headers,
                                                            params)
                as_csv_rows = json_to_csv_rows(result, image.lower(), face_attributes_set_=_face_attributes_set)
                all_results.extend(as_csv_rows)

                if sleep_time_count and image_counter % sleep_time_count == 0:
                    one_minute_tracker = sleep_if_needed(one_minute_tracker, 60)

        if image_counter % chunk_size_file == 0 or (len(all_images) - all_files == 0):
            if all_results:
                data_frame = pd.DataFrame(all_results)
                data_frame.to_csv(path_to_csv_file, chunksize=chunk_size_file, mode="a",
                                  columns=(list(all_results[0].keys())).sort()
                                  , header=first_chunk, index=False)
                all_results = []  # re-initialise the list.
                if first_chunk:
                    first_chunk = False
                print("The chunk of %d rows have been added to the file." % chunk_size_file)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-images_names_list_file', type=str, help="Path to the file which has images path in it.")
    parser.add_argument('-url', type=str, default=_URL, help="ms face api URL.")
    parser.add_argument('-keys', type=str, default=_KEY, help="list of keys to be used one by one.")
    parser.add_argument('-face_att_extract', type=str, help="Pass a comma separated string e.g. age,gender,smile")
    parser.add_argument('-face_ID', type=bool, default=False, help="True if one wants to get the faceId.")
    parser.add_argument('-face_landmarks', type=bool, default=False, help="True if face landmarks to be extracted.")
    parser.add_argument('-chunk_size', type=int, default=0,
                        help="The chunk of extracted data to be saved at once, for memory issues"
                             "if no value is passed it will save all data at once.")
    parser.add_argument('-max_call', type=int, default=132000, help="The maximum number of request with one key")
    parser.add_argument('-csv', type=str, default="face_attributes.csv",
                        help="Absolute path of the csv file to be saved.")
    parser.add_argument('-stc', type=int, default=0, help="The maximum number of requests in 60 seconds.")

    args = parser.parse_args()

    read_image_and_detect(path_to_images_name_file=args.images_names_list_file,
                          URL_=args.url,
                          keys=args.keys,
                          face_attributes_to_get_=args.face_att_extract,
                          face_landmarks=args.face_landmarks,
                          face_id=args.face_ID,
                          path_to_csv_file=args.csv,
                          maximum_calls_=args.max_call,
                          sleep_time_count=args.stc,
                          chunk_size_file=args.chunk_size)
