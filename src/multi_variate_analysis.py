import pandas as  pd
import numpy as np
import matplotlib.pyplot as plt
import csv
from sklearn.linear_model import LogisticRegression
from sklearn.naive_bayes import GaussianNB
from sklearn.tree import DecisionTreeClassifier
from sklearn.svm import LinearSVR, SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import cross_val_predict
from sklearn import metrics
from src.csv_file_reader import extract

extended_list = ["facialHair_beard", "facialHair_moustache",	"facialHair_sideburns",	"hair_bald", "hair_black", "hair_blond", "hair_brown", "hair_gray",	"hair_invisible",
                "hair_other", "hair_red", "age", "pofile_pic_count", "Single", "In a Relationship",
                "Married",	"Engaged",	"It's Complicated",	"In an Open Relationship",	"Widowed",	"Divorced",	"Separated",
                "In a domestic partnership", "In a Civil Union", "Hooked",	"Reationship_NA",	"NoGlasses", "Sunglasses",	"ReadingGlasses",
                "SwimmingGoggles","emotion_anger_de_skewed", "emotion_contempt_de_skewed", "emotion_disgust_de_skewed",
                        "emotion_fear_de_skewed", "emotion_happiness_de_skewed",
                        "emotion_neutral_de_skewed", "emotion_sadness_de_skewed", "emotion_surprise_de_skewed"]


LIST_ = ["emotion_anger_de_skewed", "emotion_contempt_de_skewed", "emotion_disgust_de_skewed",
         "emotion_fear_de_skewed", "emotion_happiness_de_skewed",
         "emotion_neutral_de_skewed", "emotion_sadness_de_skewed", "emotion_surprise_de_skewed",
         "age", "facialHair_beard", "facialHair_moustache", "facialHair_sideburns",
         "hair_bald", "hair_black", "hair_blond", "hair_brown", "hair_gray",
         "hair_other", "hair_red"]

FILES_ = ["cleaned_data_for_teresa_unique_age14_male_", "cleaned_data_for_teresa_unique_age14_female_",
              "both_gender_in_relationship_", "both_gender_not_in_relationship_", "male_not_in_relationship_",
              "male_in_relationship_", "female_in_relationship_", "female_not_in_relationship_", "cleaned_data_for_teresa_unique_age14_alles_"]

HOME_PATH = "" #put your home path


for file_name in FILES_:
    with open(HOME_PATH + "/ms_face_api/src/extended_feature_set/SVM_rbf_kernel/" +file_name +"full_quartile_predicted_traits_cv_10.csv", "w")as file_to_Save:
        csv_writer = csv.writer(file_to_Save)
        csv_writer.writerow(["Personality_traints", "Accuracy", "Precision", "Recall", "F1_Score"])
        for value in ["ope", "ext", "con", "neu", "agr"]:
            print(value)
            data_frame = pd.read_csv(
                HOME_PATH + "/ms_face_api/src/" + file_name + value + "class_25_100.csv")

            traits = ["ope", "ext", "con", "neu", "agr"]

            _emotions = ["emotion_anger_de_skewed", "emotion_contempt_de_skewed", "emotion_disgust_de_skewed",
                        "emotion_fear_de_skewed", "emotion_happiness_de_skewed",
                        "emotion_neutral_de_skewed", "emotion_sadness_de_skewed", "emotion_surprise_de_skewed"]

            # target_ope = data_frame[value + "class"]
            #data_frame_sorted = data_frame.sort_values(value)
            #row, coulmn = data_frame_sorted.shape
            #data_frame_head = data_frame_sorted.head(2000)
            # print("head : ", data_frame_head.shape)
            #data_frame_tail = data_frame_sorted.tail(2000)
            # print("tail : ", data_frame_tail.shape)

            #data_frame_head_tail = data_frame_head.append(data_frame_tail)
            target_ope = data_frame[value + "class"]
            #data_frame_emotions = extract(_emotions, data_frame)
            data_frame_emotions = extract(extended_list, data_frame)
            # print("All extreme: ", data_frame_head_tail.shape)


            #predicted = cross_val_score(LogisticRegression(), target_ope['data'], iris['target'], cv=10)
            #predicted = cross_val_predict(LogisticRegression(class_weight='balanced'), data_frame_emotions, target_ope, cv=10)
            #predicted = cross_val_predict(DecisionTreeClassifier(class_weight='balanced'), data_frame_emotions, target_ope, cv=10)
            #predicted = cross_val_predict(RandomForestClassifier(class_weight='balanced'), data_frame_emotions, target_ope, cv=10)
            #predicted = cross_val_predict(GaussianNB(), data_frame_emotions, target_ope, cv=10)
            predicted = cross_val_predict(SVC(C=1.0, cache_size=500, class_weight='balanced', coef0=0.0,
             decision_function_shape='ovr', degree=3, gamma='auto', kernel='rbf',
             max_iter=-1, probability=False, random_state=None, shrinking=True,
             tol=0.001, verbose=False), data_frame_emotions, target_ope, cv=10)
            print(metrics.confusion_matrix(target_ope, predicted))
            csv_writer.writerow([value, metrics.accuracy_score(target_ope, predicted), metrics.precision_score(target_ope, predicted),
                                 metrics.recall_score(target_ope, predicted), metrics.f1_score(target_ope, predicted)])

            print("Accuracy: ", metrics.accuracy_score(target_ope, predicted))
            print("Precision: ", metrics.precision_score(target_ope, predicted))
            print("Recall: ", metrics.recall_score(target_ope, predicted))
            print("F1_Scores : ", metrics.f1_score(target_ope, predicted))