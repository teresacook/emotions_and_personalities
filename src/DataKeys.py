CONFINDENCE = "confidence"
X = "x"
Y = "y"
FACE_ID = "faceId"


class KeysEmotions():
    emotion = "emotion"
    # scores = "scores"
    sadness = "sadness"
    neutral = "neutral"
    contempt = "contempt"
    disgust = "disgust"
    anger = "anger"
    surprise = "surprise"
    fear = "fear"
    happiness = "happiness"

    @classmethod
    def outer_key(cls):
        return cls.emotion

    @classmethod
    def all_keys(cls):
        _list = [cls.anger, cls.contempt, cls.disgust, cls.fear, cls.happiness, cls.neutral, cls.sadness, cls.surprise]
        # _set_to_check = set(_list)
        # assert len(_list) == len(_set_to_check) == 8
        return _list


class KeysFcaeRectangles():
    faceRectangle = "faceRectangle"
    width = "width"
    height = "height"
    left = "left"
    top = "top"

    @classmethod
    def outer_key(cls):
        return cls.faceRectangle

    @classmethod
    def all_keys(cls):
        _list = [cls.width, cls.height, cls.left, cls.top]
        return _list


class KeysFaceLandmarks():
    faceLandmarks = "faceLandmarks"
    pupilLeft = "pupilLeft"
    pupilRight = "pupilRight"
    noseTip = "noseTip"
    mouthLeft = "mouthLeft"
    mouthRight = "mouthRight"
    eyebrowLeftOuter = "eyebrowLeftOuter"
    eyebrowLeftInner = "eyebrowLeftInner"
    eyeLeftOuter = "eyeLeftOuter"
    eyeLeftTop = "eyeLeftTop"
    eyeLeftBottom = "eyeLeftBottom"
    eyeLeftInner = "eyeLeftInner"
    eyebrowRightInner = "eyebrowRightInner"
    eyebrowRightOuter = "eyebrowRightOuter"
    eyeRightInner = "eyeRightInner"
    eyeRightTop = "eyeRightTop"
    eyeRightBottom = "eyeRightBottom"
    eyeRightOuter = "eyeRightOuter"
    noseRootLeft = "noseRootLeft"
    noseRootRight = "noseRootRight"
    noseLeftAlarTop = "noseLeftAlarTop"
    noseRightAlarTop = "noseRightAlarTop"
    noseLeftAlarOutTip = "noseLeftAlarOutTip"
    noseRightAlarOutTip = "noseRightAlarOutTip"
    upperLipTop = "upperLipTop"
    upperLipBottom = "upperLipBottom"
    underLipTop = "underLipTop"
    underLipBottom = "underLipBottom"

    @classmethod
    def all_keys(cls):
        _list = [cls.pupilLeft, cls.pupilRight, cls.noseTip, cls.mouthLeft, cls.mouthRight, cls.eyebrowLeftOuter,
                 cls.eyebrowLeftInner, cls.eyeLeftOuter, cls.eyeLeftTop, cls.eyeLeftBottom, cls.eyeLeftInner,
                 cls.eyebrowRightInner, cls.eyebrowRightOuter, cls.eyeRightInner, cls.eyeRightTop, cls.eyeRightBottom,
                 cls.eyeRightOuter, cls.noseRootLeft, cls.noseRootRight, cls.noseLeftAlarTop, cls.noseRightAlarTop,
                 cls.noseLeftAlarOutTip, cls.noseRightAlarOutTip, cls.upperLipTop, cls.upperLipBottom, cls.underLipTop,
                 cls.underLipBottom]
        # _set_to_check = set(_list)
        # assert len(_list) == len(_set_to_check) == 27
        return _list

    x = X
    y = Y


class KeysFaceAttributes():
    age = "age"
    gender = "gender"
    smile = "smile"
    glasses = "glasses"
    faceAttributes = "faceAttributes"

    @classmethod
    def outer_key(cls):
        return cls.faceAttributes

    @classmethod
    def all_keys(cls):
        _list = [cls.age, cls.gender, cls.smile, cls.glasses]
        return _list


class KeyHeadPose():
    headPose = "headPose"
    roll = "roll"
    yaw = "yaw"
    pitch = "pitch"

    @classmethod
    def outer_key(cls):
        return cls.headPose

    @classmethod
    def all_keys(cls):
        _list = [cls.roll, cls.yaw, cls.pitch]
        # _set_to_check = set(_list)
        # assert len(_list) == len(_set_to_check) == 3
        return _list


class KeyFacialHair():
    facialHair = "facialHair"
    moustache = "moustache"
    beard = "beard"
    sideburns = "sideburns"

    @classmethod
    def outer_key(cls):
        return cls.facialHair

    @classmethod
    def all_keys(cls):
        _list = [cls.moustache, cls.beard, cls.sideburns]
        # _set_to_check = set(_list)
        # assert len(_list) == len(_set_to_check) == 3
        return _list


class KeysHairs():
    hair = "hair"
    bald = "bald"
    invisible = "invisible"
    hairColor = "hairColor"
    color = "color"
    confidence = CONFINDENCE
    '''
    {"color": "brown", "confidence": 1.0},
    {"color": "blond", "confidence": 0.88},
    {"color": "black", "confidence": 0.48},
    {"color": "other", "confidence": 0.11},
    {"color": "gray", "confidence": 0.07},
    {"color": "red", "confidence": 0.03}
    '''

    @classmethod
    def color_list(cls):
        color_value_list = ["brown", "blond", "black", "gray", "red", "other"]
        return color_value_list

    @classmethod
    def dependent_keys(cls, outer_key):
        _list = []
        if outer_key == cls.hairColor:
            _list = [cls.color, cls.confidence]
        return _list

    @classmethod
    def outer_key(cls):
        return cls.hair

    @classmethod
    def independent_key(cls):
        _list = [cls.bald, cls.invisible]
        return _list


class KeysMakeup():
    makeup = "makeup"
    eyeMakeup = "eyeMakeup"
    lipMakeup = "lipMakeup"

    @classmethod
    def outer_key(cls):
        return cls.makeup

    @classmethod
    def all_keys(cls):
        _list = [cls.eyeMakeup, cls.lipMakeup]
        return _list


class KeyAccessories():
    accessories = "accessories"
    type = "type"
    headwear = "headwear"
    glasses = "glasses"
    mask = "mask"

    confidence = CONFINDENCE

    @classmethod
    def outer_key(cls):
        return cls.accessories

    @classmethod
    def all_keys(cls):
        _list = [cls.headwear, cls.glasses, cls.mask]
        return _list


class KeyOcclusions():
    occlusion = "occlusion"
    foreheadOccluded = "foreheadOccluded"
    eyeOccluded = "eyeOccluded"
    mouthOccluded = "mouthOccluded"

    @classmethod
    def outer_key(cls):
        return cls.occlusion

    @classmethod
    def all_keys(cls):
        _list = [cls.foreheadOccluded, cls.eyeOccluded, cls.mouthOccluded]
        return _list


class KeysNoise():
    noise = "noise"
    noiseLevel = "noiseLevel"

    exposure = "exposure"
    exposureLevel = "exposureLevel"

    blur = "blur"
    blurLevel = "blurLevel"

    value = "value"
