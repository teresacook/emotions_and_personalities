import unittest
from src.DataKeys import *


class TestDataKeys(unittest.TestCase):
    def test_keys_emotions(self):
        _keys = KeysEmotions().all_keys()
        self.assertEqual(len(_keys), len(set(_keys)))

    def test_keys_facial_haris(self):
        _keys = KeyFacialHair().all_keys()
        self.assertEqual(len(_keys), len(set(_keys)))

    def test_keys_face_attributes(self):
        _keys = KeysFaceAttributes().all_keys()
        self.assertEqual(len(_keys), len(set(_keys)))

    def test_keys_face_rectangle(self):
        _keys = KeysFcaeRectangles().all_keys()
        self.assertEqual(len(_keys), len(set(_keys)))

    def test_keys_face_landmarks(self):
        _keys = KeysFaceLandmarks().all_keys()
        self.assertEqual(len(_keys), len(set(_keys)))

    def test_key_head_post(self):
        _keys = KeyHeadPose().all_keys()
        self.assertEqual(len(_keys), len(set(_keys)))

    def test_key_occlusions(self):
        _keys = KeyOcclusions().all_keys()
        self.assertEqual(len(_keys), len(set(_keys)))

    def test_key_makeyup(self):
        _keys = KeysMakeup().all_keys()
        self.assertEqual(len(_keys), len(set(_keys)))

    def test_key_accessories(self):
        _keys = KeyAccessories().all_keys()
        self.assertEqual(len(_keys), len(set(_keys)))

    def test_key_hair(self):
        _color_list = KeysHairs().color_list()
        self.assertEqual(len(_color_list), len(set(_color_list)))

        _dep_keys = KeysHairs().dependent_keys(KeysHairs().outer_key())
        self.assertEqual(len(_dep_keys), len(set(_dep_keys)))

        _ind_keys = KeysHairs().independent_key()
        self.assertEqual(len(_ind_keys), len(set(_ind_keys)))


if __name__ == '__main__':
    unittest.main()
