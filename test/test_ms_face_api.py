import unittest
from src.DataKeys import *
from src.ms_face_api import json_to_csv_rows

JSON_DATA = [{'faceId': '452479e9-a7c9-4f7b-9c9f-41e3ccfbc114',
              'faceAttributes': {'makeup': {'eyeMakeup': True, 'lipMakeup': True},
                                 'emotion': {'disgust': 0.0, 'fear': 0.0, 'surprise': 0.0, 'happiness': 0.0,
                                             'neutral': 1.0,
                                             'anger': 0.0, 'sadness': 0.0, 'contempt': 0.0},
                                 'exposure': {'exposureLevel': 'goodExposure', 'value': 0.51},
                                 'facialHair': {'beard': 0.0, 'sideburns': 0.0, 'moustache': 0.0},
                                 'blur': {'blurLevel': 'low', 'value': 0.07}, 'smile': 0.0, 'glasses': 'NoGlasses',
                                 'occlusion': {'mouthOccluded': False, 'foreheadOccluded': False,
                                               'eyeOccluded': False},
                                 'accessories': [],
                                 'hair': {'bald': 0.01, 'hairColor': [{'color': 'brown', 'confidence': 0.99},
                                                                      {'color': 'other', 'confidence': 0.92},
                                                                      {'color': 'black', 'confidence': 0.88},
                                                                      {'color': 'red', 'confidence': 0.05},
                                                                      {'color': 'gray', 'confidence': 0.04},
                                                                      {'color': 'blond',
                                                                       'confidence': 0.03}],
                                          'invisible': False}, 'age': 27.6, 'gender': 'female',
                                 'noise': {'noiseLevel': 'low', 'value': 0.0},
                                 'headPose': {'roll': -0.5, 'yaw': 2.6, 'pitch': 0.0}},
              'faceRectangle': {'top': 0, 'left': 298, 'height': 787, 'width': 815}}]

FACE_ATTRIBUTE_TO_GET = "age,gender,smile,glasses,headPose,facialHair,emotion,hair,makeup,occlusion,accessories,blur,exposure,noise"


class TestMSFaceAPI(unittest.TestCase):
    def test_json_to_csv_rows(self):
        # this test also test the methods defined in utils.py scripts
        _csv_rows = json_to_csv_rows(JSON_DATA, "test_json_data", FACE_ATTRIBUTE_TO_GET)
        single_row = _csv_rows[0]

        self.assertEqual("452479e9-a7c9-4f7b-9c9f-41e3ccfbc114", single_row.get("faceId"))
        self.assertEqual(0, single_row.get("faceRectangle_top"))
        self.assertEqual(298, single_row.get("faceRectangle_left"))
        self.assertEqual(787, single_row.get("faceRectangle_height"))
        self.assertEqual(815, single_row.get("faceRectangle_width"))

        self.assertEqual("female", single_row.get("faceAttributes_gender"))
        self.assertEqual(27.6, single_row.get("faceAttributes_age"))
        self.assertEqual(0.0, single_row.get("faceAttributes_smile"))
        self.assertEqual("NoGlasses", single_row.get("faceAttributes_glasses"))

        self.assertEqual(0.0, single_row.get("emotion_disgust"))
        self.assertEqual(0.0, single_row.get("emotion_fear"))
        self.assertEqual(0.0, single_row.get("emotion_surprise"))
        self.assertEqual(0.0, single_row.get("emotion_happiness"))
        self.assertEqual(1.0, single_row.get("emotion_neutral"))
        self.assertEqual(0.0, single_row.get("emotion_anger"))
        self.assertEqual(0.0, single_row.get("emotion_sadness"))
        self.assertEqual(0.0, single_row.get("emotion_contempt"))

        self.assertEqual(0.99, single_row.get("hair_brown"))
        self.assertEqual(0.92, single_row.get("hair_other"))
        self.assertEqual(0.88, single_row.get("hair_black"))
        self.assertEqual(0.05, single_row.get("hair_red"))
        self.assertEqual(0.04, single_row.get("hair_gray"))
        self.assertEqual(0.03, single_row.get("hair_blond"))
        self.assertEqual(0.01, single_row.get("hair_bald"))
        self.assertFalse(single_row.get("hair_invisible"))

        self.assertEqual(0.0, single_row.get("facialHair_beard"))
        self.assertEqual(0.0, single_row.get("facialHair_sideburns"))
        self.assertEqual(0.0, single_row.get("facialHair_moustache"))

        self.assertFalse(single_row.get("occlusion_mouthOccluded"))
        self.assertFalse(single_row.get("occlusion_foreheadOccluded"))
        self.assertFalse(single_row.get("occlusion_eyeOccluded"))

        self.assertEqual(-0.5, single_row.get("headPose_roll"))
        self.assertEqual(2.6, single_row.get("headPose_yaw"))
        self.assertEqual(0.0, single_row.get("headPose_pitch"))

        self.assertEqual(0.0, single_row.get("accessories_headwear"))
        self.assertEqual(0.0, single_row.get("accessories_glasses"))
        self.assertEqual(0.0, single_row.get("accessories_mask"))
        self.assertEqual(0.0, single_row.get("accessories_mask"))

        self.assertEqual(0.51, single_row.get("exposure_level"))
        self.assertEqual(0.0, single_row.get("noise_level"))
        self.assertEqual(0.07, single_row.get("blur_level"))


if __name__ == '__main__':
    unittest.main()
